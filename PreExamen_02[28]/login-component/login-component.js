export default class LoginComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.credenciales = {
            email: "",
            password: ""
        }
    }

    connectedCallback() {
        this.render();
    }

    addActionButton() {
        const boton = this.shadowRoot.querySelector("button");
        boton.addEventListener("click", () => {
            const email = this.shadowRoot.getElementById("email").value;
            const password = this.shadowRoot.getElementById("password").value;
            if (email === this.credenciales.email && password === this.credenciales.password) {
                const searchComponent = document.querySelector("search-component");
                searchComponent.style.display = "block";
                this.style.display = "none";
            } else {
                alert("Acceso denegado");
            }
        });
    }

    render() {
        this.shadowRoot.innerHTML = `
        <style>
            section{
                display: flex;
                justify-content: center;
                align-items: center;
                font-family: Verdana, sans-serif;
            }
            .login{
                background-color: white;
                width: 280px;
                padding: 20px;
                border: 1px solid blue;
                display: flex;
                justify-content: center;

            }
            label{
                font-size: 15px;
            }
            input{
                margin: 8px 0px;
                width: 100%;
                padding: 0px;
                border: 1px solid lightgray;
                height: 25px;
                border-radius: 2px;

            }
            input:focus{
                outline: none;
            }
            button{
                background-color: #66abfc;
                color: white;
                font-family: Verdana, sans-serif;
                width: 100%;
                margin-top: 5px;
                font-size: 15px;
                padding: 0px;
                border: 0px;
                height: 35px;
                border-radius: 3px;
            }
            .form{
                width: 90%;
            }
            
            #email{
                margin-bottom: 15px;
            }
            
        </style>
        <section>
            <div class="login">
                <div class="form">
                    <label for="email">Email</label>
                    <input id="email" type="email">
                    <label for="password">Password</label>
                    <input id="password" type="password">
                    <button>Login</button>
                </div>
            </div>
        </section>
        `;
        this.addActionButton();
    }
}
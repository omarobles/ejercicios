export default class SearchComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.datos = "";
    }

    connectedCallback() {
        this.render();
    }

    agregarDatos() {
        fetch("https://api.datos.gob.mx/v1/calidadAire")
            .then((results) => {
                if (!results.ok) {
                    throw new Error(`Error: ${results.status}`);
                }
                return results.json();
            })
            .then((data) => {
                data.results.forEach((element) => {
                    const tabla = this.shadowRoot.querySelector("table");
                    const fila = document.createElement("tr");
                    const tdId = document.createElement("td");
                    tdId.innerText = element.stations[0].id;
                    const tdName = document.createElement("td");
                    tdName.innerText = element.stations[0].name;
                    const tdScale = document.createElement("td");
                    tdScale.innerText = element.stations[0].indexes[0].scale;
                    const tdFecha = document.createElement("td");
                    tdFecha.innerText = element.stations[0].indexes[0].calculationTime;
                    fila.appendChild(tdId);
                    fila.appendChild(tdName);
                    fila.appendChild(tdScale);
                    fila.appendChild(tdFecha);
                    tabla.appendChild(fila);
                })
            })
            .catch((error) => {
                const tabla = this.shadowRoot.querySelector("table");
                tabla.innerHTML = ``;
                const fila = document.createElement("tr");
                const tdError = document.createElement("td");
                tdError.innerText = `Ocurrió un error: ${error}`;
                fila.appendChild(tdError);
                tabla.appendChild(fila);
            });
    }

    addEventSearch() {
        const input = this.shadowRoot.getElementById("busqueda");
        const btnBuscar = this.shadowRoot.getElementById("buscar");
        const tablaResultados = this.shadowRoot.getElementById("tabla-resultados");

        btnBuscar.addEventListener("click", () => {
            tablaResultados.innerHTML = "";
            const filaTH1 = document.createElement("tr");
            const thLocalidad = document.createElement("th");
            const thIndices = document.createElement("th");
            const thMediciones = document.createElement("th");
            const thNombre = document.createElement("th");
            const thId = document.createElement("th");
            const thEscala = document.createElement("th");
            const thValor = document.createElement("th");
            const thFecha = document.createElement("th");
            const thValorMed = document.createElement("th");
            const thUnidad = document.createElement("th");
            const thContam = document.createElement("th");

            thLocalidad.innerText = `Localidad`;
            thIndices.innerText = `Índices`;
            thMediciones.innerText = `Mediciones`;
            thNombre.innerText = `Nombre`;
            thId.innerText = `Id`;
            thEscala.innerText = `Escala`;
            thValor.innerText = `Valor`;
            thFecha.innerText = `Fecha`;
            thValorMed.innerText = `Valor`;
            thUnidad.innerText = `Unidad`;
            thContam.innerText = `Contaminación`;
            thLocalidad.setAttribute("colspan", 2);
            thIndices.setAttribute("colspan", 3);
            thMediciones.setAttribute("colspan", 3);

            filaTH1.appendChild(thLocalidad)
            tablaResultados.appendChild(filaTH1);
            filaTH1.appendChild(thIndices)
            tablaResultados.appendChild(filaTH1);
            filaTH1.appendChild(thMediciones)
            tablaResultados.appendChild(filaTH1);
            const filaTH2 = document.createElement("tr");
            filaTH2.appendChild(thNombre);
            filaTH2.appendChild(thId);
            filaTH2.appendChild(thEscala);
            filaTH2.appendChild(thValor);
            filaTH2.appendChild(thFecha);
            filaTH2.appendChild(thValorMed);
            filaTH2.appendChild(thUnidad);
            filaTH2.appendChild(thContam);
            tablaResultados.appendChild(filaTH2);
            const localidad = input.value.trim();
            fetch("https://api.datos.gob.mx/v1/calidadAire")
                .then((results) => {
                    return results.json();
                }).then((data) => {
                    data.results.forEach((element) => {
                        if (localidad === element.stations[0].name) {
                            const filaDatos = document.createElement("tr");
                            const tdId = document.createElement("td");
                            tdId.innerText = element.stations[0].id;
                            const tdName = document.createElement("td");
                            tdName.innerText = element.stations[0].name;
                            const tdScale = document.createElement("td");
                            tdScale.innerText = element.stations[0].indexes[0].scale;
                            const tdValue = document.createElement("td");
                            tdValue.innerText = element.stations[0].indexes[0].value;
                            const tdFecha = document.createElement("td");
                            tdFecha.innerText = element.stations[0].indexes[0].calculationTime;
                            const tdValueMed = document.createElement("td");
                            tdValueMed.innerText = element.stations[0].measurements[0]?.value !== undefined ? element.stations[0].measurements[0].value : "No definido";
                            const tdUnit = document.createElement("td");
                            tdUnit.innerText = element.stations[0].measurements[0]?.unit !== undefined ? element.stations[0].measurements[0].unit : "No definido";
                            const tdCont = document.createElement("td");
                            tdCont.innerText = element.stations[0].measurements[0]?.pollutant !== undefined ? element.stations[0].measurements[0].pollutant : "No definido";
                            filaDatos.appendChild(tdId);
                            filaDatos.appendChild(tdName);
                            filaDatos.appendChild(tdScale);
                            filaDatos.appendChild(tdFecha);
                            filaDatos.appendChild(tdValue);
                            filaDatos.appendChild(tdFecha);
                            filaDatos.appendChild(tdValueMed);
                            filaDatos.appendChild(tdUnit);
                            filaDatos.appendChild(tdCont);
                            tablaResultados.appendChild(filaDatos);
                        }
                    })
                    if (localidad === "") {
                        const filaMensaje = document.createElement("tr");
                        const tdMensaje = document.createElement("td");
                        tdMensaje.innerText = "No existen registros de esa localidad";
                        tdMensaje.colSpan = 8;
                        filaMensaje.appendChild(tdMensaje);
                        tablaResultados.appendChild(filaMensaje);
                    }
                })
        })
    }

    addLimpiar() {
        const btnLimpiar = this.shadowRoot.getElementById("limpiar");
        btnLimpiar.addEventListener("click", () => {
            const input = this.shadowRoot.getElementById("busqueda");
            const tablaResultados = this.shadowRoot.getElementById("tabla-resultados");
            input.value = "";
            tablaResultados.innerText = "";
        })
    }

    render() {
        this.shadowRoot.innerHTML = `
        <style>
            section{
                font-family: Verdana, sans-serif;
                display: flex;
                justify-content: center;
                text-align: start;
            }
            .container{
                width: 80%;
            }
            .tabla{
                max-height: 400px;
                overflow: auto;
            }
            table{
                width: 100%;
                margin: 10px auto;
                border: 1px solid lightgray;
            }
            th{
                background-color: lightgray;
            }
            td{
                border-bottom: 1px solid lightgray;
            }
            tr{
                text-align: center;
            }
            #busqueda{
                width: 250px;
                font-family: Verdana, sans-serif;
            }
            #resultados{
                margin-top: 50px;
                width: 100%;
                overflow: auto;
            } 
        </style>
        <section>
            <div class="container">
                    <label for="busqueda">Filtrar:</label>
                    <input id="busqueda" type="text" placeholder="Ingresa el nombre de localidad">
                    <button id="buscar">Buscar</button>
                    <button id="limpiar">Limpiar</button>
                <div class="tabla">
                    <table>
                        <tr>
                            <th>id</th>
                            <th>Localidad</th>
                            <th>Escala</th>
                            <th>Fecha</th>
                        </tr>
                    </table>
                </div>
                <div id="resultados">
                <h1>Resultados</h1>
                    <table id="tabla-resultados">
                    </table>
                </div>
            </div>
        </section>
        `;
        this.agregarDatos();
        this.addEventSearch();
        this.addLimpiar();
    }
}
import { html, css, LitElement } from 'lit';

export class EContact extends LitElement {
  static styles = css`
    div {
      border: 1px solid #ddd;
      padding: 10px;
      border-radius: 5px;
      display: inline-block;
    }
    h1 {
      font-size: 1.2rem;
      font-weight: normal;
    }
  `;

  static properties = {
    nombre: { type: String },
    email: { type: String },
    verMas: { type: Boolean }
  };

  constructor() {
    super();
    this.verMas = false;
  }

  toggle(e){
    e.preventDefault();
    this.verMas = !this.verMas;
  }

  render() {
    return html`
      <div>
        <h1>${this.nombre}</h1>
        <p>
          <a href="#" @click ="${this.toggle}">Ver más</a>
        </p>
        ${this.verMas ?
        html`Email: ${this.email}` : ''
      }
      </div>
    `;
  }
}

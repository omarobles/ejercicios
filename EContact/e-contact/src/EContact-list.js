import { html, LitElement } from "lit";
import '../e-contact.js';

export class EContactList extends LitElement {
    constructor() {
        super();
        this.contactos = [
            {
                nombre: 'Lucho Godínez',
                email: 'user1_some_email@mail.com'
            },
            {
                nombre: 'Hugo Sánchez',
                email: 'user2_some_email@mail.com'
            },
            {
                nombre: 'Jhon Doe',
                email: 'user3_some_email@mail.com'
            },
        ]
    }

    static properties = {
        contactos: { type: Array }
    }

    render() {
        return html`
        <div>
            ${this.contactos.map(contacto => 
            html`
                <e-contact nombre="${contacto.nombre}" email="${contacto.email}"></e-contact>
            `
        )}
        </div>
        `;
    }
}
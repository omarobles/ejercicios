import { EContact } from './src/EContact.js';
import { EContactList } from './src/EContact-list.js';
window.customElements.define('e-contact', EContact);
window.customElements.define('e-contact-list', EContactList);

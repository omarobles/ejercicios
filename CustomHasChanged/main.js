import { LitElement, html } from 'lit';

export class CustomHasChanged extends LitElement {
    constructor() {
        super();
        this.myProp = 1;
    }

    static properties = {
        myProp: {
            type: Number,
            hasChanged(newVal, oldVal) {
                if (newVal > oldVal) {
                    console.log(`${newVal} > ${oldVal}. Has Changed: true`);
                    return true;
                }
                else {
                    console.log(`${newVal} <= ${oldVal}. Has Changed: false`);
                    return false;
                }
            }
        }
    }

    getNewVal() {
        let newVal = Math.floor(Math.random() * 10);
        this.myProp = newVal;
    }

    updated() {
        console.log('Updated');
    }

    render() {
        return html`
        <p>myProp: ${this.myProp}</p>
        <button @click="${this.getNewVal}"> get new val</button>
        `;
    }
}

customElements.define('custom-has-changed', CustomHasChanged);
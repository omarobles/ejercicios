import { LitElement, html } from 'lit';
export class MyElement extends LitElement {
    render() {
        return html`
        <div>
            <slot name="one"></slot>
            <slot name="two"></slot>
        </div>
        `;
    }
}
var personaArr = [
    {
        personaId: 123,
        name: "Jhon",
        city: "Melbourne",
        phoneNo: "1234567890"
    },
    {
        personaId: 124,
        name: "Amelia",
        city: "Sydney",
        phoneNo: "1234567890"
    },
    {
        personaId: 125,
        name: "Emily",
        city: "Perth",
        phoneNo: "1234567890"
    },
    {
        personaId: 126,
        name: "Abraham",
        city: "Perth",
        phoneNo: "1234567890"
    },
];
const registro = personaArr.map((persona) => {
    return `
        <tr>
            <td>${persona.personaId}</td>
            <td>${persona.name}</td>
            <td>${persona.city}</td>
            <td>${persona.phoneNo}</td>
        </tr>
    `;
}).join("");
// document.body.innerHTML = `
//     <table>
//         <tr>
//             <th>Person Id</th>
//             <th>Name</th>
//             <th>City</th>
//             <th>Phone No.</th>
//         </tr>
//         ${registro}
//     </table>
// `;

//Ejercicio 2
const arregloLi = document.querySelectorAll("li");
arregloLi.forEach(elemento => {
    elemento.addEventListener('click', ev => {
        alert(`Elemento seleccionado: \nID elemento: ${elemento.id}\nISO ID: ${elemento.getAttribute("data-id")}\nDial Code: ${elemento.getAttribute("data-dial-code")}`);
    });
});

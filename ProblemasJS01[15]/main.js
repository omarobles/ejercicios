//Ejercicio 1
const clientes = ["Luis", "Omar", "Gloria"], empleados = ["Epifanio", "Martín", "Joselyn"];
let contactos;
//a. Concat
contactos = clientes.concat(empleados);
console.log(contactos);

//b. Join
contactos = clientes.join(empleados);
console.log(contactos);

//c. Push
contactos = clientes.push(empleados);
console.log(contactos, clientes);

//d. Splice
contactos = clientes.splice(empleados)
console.log(contactos);

//Ejercicio 2 
console.log("=======================================")
console.log("Ejercicio 2")
var numbers = [5, 32, 43, 4];
const resultadoFilter = numbers.filter(function (n) { return n % 2 !== 0 });
console.log(resultadoFilter);

//Ejercicio 3
console.log("=======================================")
console.log("Ejercicio 3")
var people = [{
    id: 1,
    name: "John",
    age: 28,
}, {
    id: 2,
    name: "Jane",
    age: 31
}, {
    id: 3,
    name: "Peter",
    age: 55
}
];
console.log(people.filter((person) => {
    return person.age < 35;
}));

//Ejercicio 4
console.log("=======================================");
console.log("Ejercicio 4");
let people2 = [
    { name: "bob", id: 1 },
    { name: "john", id: 2 },
    { name: "alex", id: 3 },
    { name: "john", id: 3 }
];

const numVeces = array => {
    for (let i = 0; i < array.length; i++) {
        console.count(array[i].name);
    }
}
numVeces(people2);

//Ejercicio 5
console.log("=======================================")
console.log("Ejercicio 5");
var myArray = [1, 2, 3, 4];
const maxMin = array => {
    console.log(Math.max(...array), Math.min(...array));
};
maxMin(myArray);

//Ejercicio 6
console.log("=======================================")
console.log("Ejercicio 6");
var object = {
    key1: 10,
    key2: 3,
    key3: 40,
    key4: 20
}

const arregloKey = ob => {
    const arreglo = Object.values(ob);
    return arreglo.sort((a, b) => a - b);
};

console.log(arregloKey(object));
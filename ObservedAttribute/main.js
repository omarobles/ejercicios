import { LitElement, html } from 'lit';

export class ObservedAttributes extends LitElement {

    constructor() {
        super();
        this.myProp = 'myProp';
        this.theProp = 'theProp';
        this.otherProp = 'otherProp';
    };

    static properties = {
        myProp: { attribute: false },
        theProp: { attribute: false },
        otherProp: { attribute: 'other-prop' }
    };

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('attribute change: ', name, newValue);
        super.attributeChangedCallback(name, oldValue, newValue);
    }

    changeAttribute() {
        let randomString = Math.floor(Math.random() * 100).toString();
        this.setAttribute('myProp', 'myProp ' + randomString);
        this.setAttribute('theProp', 'theProp ' + randomString);
        this.setAttribute('other-prop', 'other-prop ' + randomString);
        this.requestUpdate();
    }

    updated(changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log(`${propName} changed. oldValue: ${oldValue}`);
        })
    }

    render() {
        return html`
        <p>myProp ${this.myProp}</p>
        <p>theProp ${this.theProp}</p>
        <p>otherProp ${this.otherProp}</p>
        <button @click="${this.changeAttribute}">change attribute</button>
        `;
    }
}

customElements.define('observed-attribute', ObservedAttributes);
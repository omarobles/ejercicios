import { TextInput } from "./text-input.js";
customElements.define('my-text-input', TextInput);

import { LitElement, html, css } from "lit";

export class InputSample extends LitElement {
    static styles = css`
    :host {
        display: block;
        padding: 25px;
        color: var(--input-sample-text-color, #000);
    }
    `;

    static properties = {
        miDato: { type: String }
    }

    constructor() {
        super();
        this.miDato = 'Valor de inicialización';
    }

    inputCambiado(e) {
        this.miDato = e.detail;
    }
    resetTexto() {
        this.miDato = '';
    }

    render() {
        return html`
        <p>Soy My Element</p>
        <my-text-input .value = ${this.miDato} @change="${this.inputCambiado}"></my-text-input>
        <p>El dato escrito es ${this.miDato}</p>
        <button @click=${this.resetTexto}>Borrar texto</button>
        `;
    }
}
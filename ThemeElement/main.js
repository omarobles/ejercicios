import { html, css, LitElement } from 'lit';
export class ThemeElement extends LitElement {
    static styles = css`
    :host {
        display: block;
        color: var(--my-element-text-color, black);
        background-color: var(--my-element-background-color, white);
        font-family: var(--my-element-font-family, Roboto);
    }

    :host([hidden]){
        display: none;
    }
    `;

    render() {
        return html`
        <div>
            <p>
                Lorem ipsum dolor sit, amet consectetur adipsici elit. Voluptatibus...
            </p>
        </div>
        `;
    }
}

customElements.define('theme-element', ThemeElement);
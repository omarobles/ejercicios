// const resultado = document.querySelector("h3");
// document.addEventListener("mousemove", event => {
//     resultado.innerText = `Página [${event.pageX}, ${event.pageY}]`;
// })

// document.addEventListener("keyup", event => {
//     resultado.innerText = `Carácter [${event.key}]`;
// })

const uno = document.getElementById("1");
const dos = document.getElementById("2");
const tres = document.getElementById("3");
const cuatro = document.getElementById("4");
const cinco = document.getElementById("5");
const seis = document.getElementById("6");
const siete = document.getElementById("7");
const ocho = document.getElementById("8");
const nueve = document.getElementById("9");
const cero = document.getElementById("0");
const punto = document.getElementById("punto");
const igual = document.getElementById("igual");

const suma = document.getElementById("suma");
const resta = document.getElementById("resta");
const mult = document.getElementById("mult");
const div = document.getElementById("division");

const input = document.getElementById("entrada");
const borrar = document.querySelector(".borrar");
const progreso = document.getElementById("progreso");

let operacion, n1 = null, n2 = null;

uno.addEventListener("click", () => {
    input.value += 1;
    progreso.textContent += 1;
})
dos.addEventListener("click", () => {
    input.value += 2;
    progreso.textContent += 2;
})
tres.addEventListener("click", () => {
    input.value += 3;
    progreso.textContent += 3;
})
cuatro.addEventListener("click", () => {
    input.value += 4;
    progreso.textContent += 4;
})
cinco.addEventListener("click", () => {
    input.value += 5;
    progreso.textContent += 5;
})
seis.addEventListener("click", () => {
    input.value += 6;
    progreso.textContent += 6;
})
siete.addEventListener("click", () => {
    input.value += 7;
    progreso.textContent += 7;
})
ocho.addEventListener("click", () => {
    input.value += 8;
    progreso.textContent += 8;
})
nueve.addEventListener("click", () => {
    input.value += 9;
    progreso.textContent += 9;
})
cero.addEventListener("click", () => {
    input.value += 0;
    progreso.textContent += 0;
})
punto.addEventListener("click", () => {
    input.value += ".";
    progreso.textContent += ".";
})

suma.addEventListener("click", () => {
    n1 = input.value;
    operacion = "+";
    input.value = "";
    progreso.textContent += operacion;
})
resta.addEventListener("click", () => {
    n1 = input.value;
    operacion = "-";
    input.value = "";
    progreso.textContent += operacion;
})
mult.addEventListener("click", () => {
    n1 = input.value;
    operacion = "*";
    input.value = "";
    progreso.textContent += operacion;
})
div.addEventListener("click", () => {
    n1 = input.value;
    operacion = "/";
    input.value = "";
    progreso.textContent += operacion;
})

igual.addEventListener("click", () => {
    n1 = parseFloat(n1);
    n2 = parseFloat(input.value);
    let result;
    if (operacion === "+") {
        result = n1 + n2;
    } else if (operacion === "-") {
        result = n1 - n2;
    } else if (operacion === "*") {
        result = n1 * n2;
    } else if (operacion === "/") {
        result = n1 / n2;
    }
    progreso.textContent += ` = ${result}`;
    n1 = null;
    n2 = null;
    operacion = "";
    input.value = result;
});

borrar.addEventListener("click", function () {
    input.value = "";
    progreso.textContent = "";
})

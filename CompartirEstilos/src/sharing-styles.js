import { html, css, LitElement } from 'lit';
import { buttonStyles } from './_module-styles.js';

export class SharingStyles extends LitElement {
    static styles = [
        buttonStyles,
        css`
        :host {
            display: block;
            border: 1px solid black;
        }
        `
    ];

    render() {
        return html`
        <button class="blue-button">click</button>
        <button class="blue-button" disabled>no click</button>
        `;
    }
}
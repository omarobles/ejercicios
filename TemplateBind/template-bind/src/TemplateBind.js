import { html, css, LitElement } from 'lit';

export class TemplateBind extends LitElement {
  static styles = css`
    :host {
      display: block;
      padding: 25px;
      color: var(--template-bind-text-color, #000);
    }
  `;

  static properties = {
    prop1: { type: String },
    prop2: { type: String },
    prop3: { type: Boolean },
    prop4: { type: String },
    activo: { type: Boolean }
  };

  constructor() {
    super();
    this.prop1 = 'text binding';
    this.prop2 = 'mydiv';
    this.prop3 = true;
    this.prop4 = 'pie';
    this.activo = true;
  }

  render() {
    return html`
      <!-- text binding --> 
      <div>${this.prop1}</div>
      
      <!-- attribute binding --> 
      <div id="${this.prop2}">Attribute binding</div>

      <!-- boolean attribute binding --> 
      <div>
        Boolean attribute binding
        <input type="text" ?disabled="${this.prep3}">
      </div>

      <!-- property binding --> 
      <div>
        Property binding
        <input type="text" .value="${this.prop4}">
      </div>

      <!-- event handler binding --> 
      <div>
        Event handler binding
        <button @click="${this.clickHandler}">click</button>
      </div>

      <!-- Manejador de eventos check -->
      <div>
        Manejador de eventos check
        <p><input type="checkbox" @change="${this.doChange}" ?checked="${this.activo}">check ??</p>
      </div> 
    `;
  }

  clickHandler(e) {
    console.log(e.target);
  }
  doChange(e){
    this.activo = e.target.checked;
    console.log(`Cambiar activo a: ${this.activo}`);
  }
}

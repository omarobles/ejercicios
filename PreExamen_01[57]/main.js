const input = document.querySelector("input");
const boton = document.querySelector("button");
const lista = document.getElementById("lista");
const textoCambio = document.getElementById("cambio");
const compraNueva = document.getElementById("compra-nueva");
const salir = document.getElementById("salir");
const aceptar = document.getElementById("aceptar");
let usuario = "", producto = "", precio = 0, cambio = 0, tipoInput = "usuario", registroCambio = "", numOperaciones = 0, comprasRealizadas = 0;
boton.addEventListener("click", () => {
    if (tipoInput === "usuario") {
        tipoInput = "producto";
        usuario = input.value;
        input.setAttribute("placeholder", "Ingresa el producto");
        input.value = "";
    } else if (tipoInput === "producto") {
        tipoInput = input.value === "A" || input.value === "B" || input.value === "C" ? "monedas" : "producto";
        producto = input.value;
        input.setAttribute("placeholder", "Ingresa la cantidad");
        input.value = "";
        switch (producto) {
            case "A":
                precio = 270;
                lista.textContent = `Producto ${producto} seleccionado, ingrese la cantidad: $${precio}`;
                break;
            case "B":
                precio = 340;
                lista.textContent = `Producto ${producto} seleccionado, ingrese la cantidad: $${precio}`;
                break;
            case "C":
                precio = 390;
                lista.textContent = `Producto ${producto} seleccionado, ingrese la cantidad: $${precio}`;
                break;
            default:
                input.setAttribute("placeholder", "Ingresa un producto válido");
                lista.textContent = `Producto inválido - Disponibles: A. $270 - B. $340 - C. $390`;
                break;
        }
    } else if (tipoInput === "monedas") {
        if (input.value === "10" || input.value === "50" || input.value === "100") {
            if (input.value <= precio) {
                precio -= input.value;
                lista.textContent = `Producto ${producto} seleccionado, ingrese la cantidad: $${precio}`;
                numOperaciones++;
            } else {
                cambio = input.value - precio;
                lista.textContent = `Producto adquirido, cambio: ${cambio}`;
                while (cambio > 0) {
                    if (cambio % 100 === 0) { registroCambio += " 100"; cambio -= 100; }
                    else if (cambio % 50 === 0) { registroCambio += " 50"; cambio -= 50; }
                    else if (cambio % 10 === 0) { registroCambio += " 10"; cambio -= 10; }
                }
                comprasRealizadas++;
                textoCambio.textContent = `${registroCambio}`;
                compraNueva.style.display = "block";
                salir.style.display = "block";
                input.style.display = "none";
                aceptar.style.display = "none";
            }
        }
        else input.setAttribute("placeholder", "Ingresa una moneda válida");
        input.value = "";
    }
});

compraNueva.addEventListener("click", () => {
    input.setAttribute("placeholder", "Ingresa el usuario");
    lista.textContent = "A. $270 - B. $340 - C. $390";
    textoCambio.textContent = "";
    usuario = "";
    cambio = 0;
    producto = "";
    precio = 0;
    registroCambio = "";
    tipoInput = "usuario";
    compraNueva.style.display = "none";
    salir.style.display = "none";
    aceptar.style.display = "block";
    input.style.display = "block";
})

salir.addEventListener("click", () => {
    compraNueva.style.display = "none";
    textoCambio.textContent = "";
    salir.style.display = "none";
    aceptar.style.display = "none";
    input.style.display = "none";
    lista.textContent = `Compras realizadas: ${comprasRealizadas} - Operaciones realizadas ${numOperaciones}`
})


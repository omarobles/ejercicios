import { LitElement, html, css } from "lit";

export class SuperElemento extends LitElement {
    static styles = css`
    button{
        width: 300px;
        font-style: italic;
    }
    `;

    render() {
        return html`
        <button>click</button>
        `;
    }
}
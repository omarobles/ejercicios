import { LitElement, css } from "lit";
import { SuperElemento } from "./super-elemento.js";

export class HerenciaElemento extends SuperElemento {
    static styles = [
        super.styles,
        css `
        button { color: red}
        `
    ];
}
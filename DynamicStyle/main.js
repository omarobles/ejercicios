import { html, css, LitElement } from 'lit';
import {classMap} from 'lit/directives/class-map.js';
import {styleMap} from 'lit/directives/style-map.js';

export class DynamicStyle extends LitElement {
    constructor() {
        super();
        this.classes = { mydiv: true, someclass: true };
        this.styles = { color: 'green', fontFamily: 'Roboto' };
    }

    static properties = {
        classes: { type: Object },
        styles: { type: Object }
    }

    static styles = css`
    .mydiv {background-color: blue;}
    .someclass {border: 1px solid red}
    `;

    render() {
        return html`
        <div class=${classMap(this.classes)} style=${styleMap(this.styles)}>
            Some content
        </div>
        `;
    }
}

customElements.define('dynamic-style', DynamicStyle);
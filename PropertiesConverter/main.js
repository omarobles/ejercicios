import { LitElement, html } from "lit";

export class MyConverter extends LitElement {
    constructor() {
        super();
        this.myProp = 'myProp';
        this.theProp = 'theProp';
    }
    static properties = {
        myProp: {
            reflect: true,
            converter: {
                toAttribute(value) {
                    console.log('myProps toAttribute');
                    console.log('Processing: ', value, typeof (value));
                    let retVal = String(value);
                    console.log('Returning: ', retVal, typeof (retVal));
                    return retVal;
                },
                fromAttribute(value) {
                    console.log('myProps fromAttribute. ');
                    console.log('Processing: ', value, typeof (value));
                    let retVal = Number(value);
                    console.log('Returning: ', retVal, typeof (retVal));
                    return retVal;
                }
            }
        },
        theProp: {
            reflect: true,
            converter(value) {
                console.log('theProps converter');
                console.log('Processing: ', value, typeof (value));
                let retVal = Number(value);
                console.log('Returning: ', retVal, typeof (retVal));
                return retVal;
            }
        }
    }
    attributeChangedCallback(name, oldVal, newVal) {
        console.log('attribute changes: ', name, newVal);
        super.attributeChangedCallback(name, oldVal, newVal);
    }

    changeAttributes() {
        let randomString = Math.floor(Math.random() * 100).toString();
        this.setAttribute('myProp', 'myProp ' + randomString);
        this.setAttribute('theProp', 'theProp ' + randomString);
        this.requestUpdate();
    }

    changeProperties() {
        let randomString = Math.floor(Math.random() * 100).toString();
        this.myProp = 'myProp ' + randomString;
        this.theProp = 'theProp ' + randomString;
    }

    render() {
        return html`
        <p>myProp: ${this.myProp} tipo: ${typeof (this.myProp)}</p>
        <p>theProp: ${this.theProp} tipo: ${typeof (this.theProp)}</p>
        <button @click="${this.changeProperties}">change properties</button>
        <button @click="${this.changeAttributes}">change attributes</button>
        `;
    }

}

customElements.define('my-converter', MyConverter);